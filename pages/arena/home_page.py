from selenium.webdriver.common.by import By


class HomePage:

    def __init__(self, browser):
        self.browser = browser

    def logout(self):
        self.browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').click()

    def admin_projekty(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()

