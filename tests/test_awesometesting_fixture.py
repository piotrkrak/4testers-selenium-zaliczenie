import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture
def browser():
    # cześć która wykona się przed każdym testem
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # coś co przekażemy do każdego testu
    yield browser

    # cześć która wykona się przed po testem
    browser.close()


def test_post_count(browser):
    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')
    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 4 elementy
    assert len(posts) == 4


def test_post_count_after_search(browser):
    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')
    searchbar = browser.find_element(By.CSS_SELECTOR, '[title = search]')
    search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')
    searchbar.send_keys('selenium')
    search_button.click()
    time.sleep(4)
    findposts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    assert len(findposts) == 20


def test_post_count_on_cypress_label(browser):
    browser.get('http://awesome-testing.blogspot.com/')
    label = browser.find_element(By.LINK_TEXT, '2019')
    label.click()
    time.sleep(4)
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    assert len(posts) == 10
