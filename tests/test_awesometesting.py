import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_post_count():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    # browser = Chrome(executable_path=ChromeDriverManager().install())
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 4 elementy
    assert len(posts) == 4
    # Zamknięcie przeglądarki
    browser.close()

def test_post_count_after_search():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    # browser = Chrome(executable_path=ChromeDriverManager().install())
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')
    # Inicjalizacja searchbara i przycisku search
    searchbar = browser.find_element(By.CSS_SELECTOR, '[title = search]')
    search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')
    # Szukanie
    searchbar.send_keys('selenium')
    search_button.click()
    # Czekanie na stronę
    time.sleep(4)
    # Pobranie listy tytułów
    findposts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 20 elementy
    assert len(findposts) == 20
    # Zamknięcie przeglądarki
    browser.close()

def test_post_count_on_cypress_label():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    # browser = Chrome(executable_path=ChromeDriverManager().install())
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')
    # Inicjalizacja elementu z labelką
    label = browser.find_element(By.LINK_TEXT, '2019')
    # Kliknięcie na labelkę
    label.click()
    # Czekanie na stronę
    time.sleep(4)

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 1 element
    assert len(posts) == 10
    # Zamknięcie przeglądarki
    browser.close()
