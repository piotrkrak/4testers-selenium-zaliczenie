import time

from selenium.webdriver import Chrome
from selenium.webdriver import Edge
from selenium.webdriver.chrome.service import Service
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.chrome import ChromeDriverManager

# Test - uruchomienie Chroma


def test_my_first_chrome_selenium_test():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://demo.testarena.pl/zaloguj')

    browser.maximize_window()
    time.sleep(4)

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'TestArena' in browser.title

    # Zamknięcie przeglądarki
    browser.quit()


# Test - uruchomienie Edge
def test_my_first_edge_selenium_test():
    # Uruchomienie przeglądarki Edge. Ścieżka do geckodrivera (drivera dla Firefoxa)
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(EdgeChromiumDriverManager().install())
    browser = Edge(service=service)

    # Otwarcie strony www.google.pl

    # Weryfikacja tytułu

    # Zamknięcie przeglądarki
    browser.quit()


def test_chrome_selenium_add_cookie():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://awesome-testing.blogspot.com')
    browser.add_cookie({"name": "displayCookieNotice", "value": "y"})
    browser.refresh()

    browser.set_window_size(1920, 1080)
    time.sleep(4)

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'Software testing Blog – Awesome Testing' in browser.title

    # Zamknięcie przeglądarki
    browser.quit()