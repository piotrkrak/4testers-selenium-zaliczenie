import time

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony duckduckgo
    browser.get('https://duckduckgo.com')

    # Znalezienie paska wyszukiwania
    searchbox_input = browser.find_element(By.CSS_SELECTOR, '#searchbox_homepage')

    # Znalezienie guzika wyszukiwania (lupki)
    searchicon = browser.find_element(By.CSS_SELECTOR, '[aria-label=Search]')

    # Asercje że elementy są widoczne dla użytkownika

    # Szukanie 4Testers
    searchbox_input.send_keys('4Testerss')

    # Sprawdzenie że pierwszy wynik ma tytuł '4Testers'

    # Zamknięcie przeglądarki
    time.sleep(4)
    browser.quit()
